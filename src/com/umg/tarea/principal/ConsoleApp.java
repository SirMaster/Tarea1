package com.umg.tarea.principal;

import com.umg.tarea.clases.Actividad;
import com.umg.tarea.clases.Campus;
import com.umg.tarea.clases.Colaborador;

import javax.swing.*;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by Guicho on 5/07/2017.
 */
public class ConsoleApp {

    public static void main(String[] args) throws Exception {

        Campus campus = null;

        Scanner sn = new Scanner(System.in);
        boolean salir = false;
        int opcion;

        while (!salir){
            System.out.println("Seleccione una de las opciones");
            System.out.println("1: Ingreso Actividades");
            System.out.println("2: Ingreso Colaborador");
            System.out.println("3: Listar actividades en el Campus");
            try {
                opcion = sn.nextInt();
                switch (opcion) {
                    case 1:
                        System.out.println("Asigne numero de actividades");
                        campus = new Campus(sn.nextInt());
                        System.out.println(" ***Actividades ingresadas = "+ campus.getCantActiv()+" ***");
                        for(int i=0; i<campus.getCantActiv(); i++){
                            System.out.println("Nombre de la Actividad #"+(i+1));
                            Actividad actividad = new Actividad(sn.next());
                            System.out.println("¿Es dentro del Campus? (S/N)");
                                if (sn.next().equalsIgnoreCase( "S"))
                                    actividad.setActDentro(true);
                                campus.adicionarActividad(actividad);
                        }
                        break;
                    case 2:
                        System.out.println("Nombre");
                        Colaborador colaborador = new Colaborador();
                        colaborador.setNombreColaborador(sn.next());
                        System.out.println("Edad");
                        colaborador.setEdad(sn.nextInt());
                        break;
                    case 3:
                        Actividad[] listado = campus.getListado();
                        for (int i = 0; i <campus.getCantReal() ; i++) {
                            if(listado[i].isActDentro())
                                System.out.printf("Actividad  dentro del campus# "+(i+1)+ ": "+listado[i].getNombreActividad()+"\n");
                        }
                        System.out.println("-----------------");
                        break;
                    default:
                        System.out.println("Solo números del 1 al 3");
                }

            } catch (InputMismatchException e) {
                System.out.println("Debes ingresar un numero");
                sn.next();
            }


        }
    }
}
