package com.umg.tarea.clases;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Guicho on 5/07/2017.
 */
public class Campus {
    private Actividad[] listado;
    private List<Actividad> listadoActividad;
    private Colaborador colaboradorn;
    private int cantReal;
    public int cantActiv;

    public Campus(int cantActividades) {
        listado = new Actividad[cantActividades];
        cantReal = 0;
        cantActiv = cantActividades;
        listadoActividad = new ArrayList<>();
    }

    public int getCantActiv() {
        return cantActiv;
    }

    public void setCantActiv(int cantActiv) {
        this.cantActiv = cantActiv;
    }

    public Actividad[] getListado() {
        return listado;
    }

    public void setListado(Actividad[] listado) {
        this.listado = listado;
    }

    public List<Actividad> getListadoActividad() {
        return listadoActividad;
    }

    public void setListadoActividad(List<Actividad> listadoActividad) {
        this.listadoActividad = listadoActividad;
    }

    public Colaborador getColaboradorn() {
        return colaboradorn;
    }

    public void setColaboradorn(Colaborador colaboradorn) {
        this.colaboradorn = colaboradorn;
    }

    public int getCantReal() {
        return cantReal;
    }

    public void setCantReal(int cantReal) {
        this.cantReal = cantReal;
    }

    public void adicionarActividad(Actividad a)throws Exception {

        if (cantReal < listado.length){

            if (a.isActDentro()){
                for (int i = cantReal; i > 0; i--) {
                    listado[i] = listado[i - 1];
                }
                listado[0] = a;
            } else
                listado[cantReal] = a;
            cantReal++;
        }else
            throw new Exception("Imposiple adicionar mas actividades");
    }

}
