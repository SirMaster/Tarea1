package com.umg.tarea.clases;


/**
 * Created by Guicho on 5/07/2017.
 */
public class Colaborador {
    private String nombreColaborador;
    private int edad;

    public Colaborador() {
    }

    public String getNombreColaborador() {
        return nombreColaborador;
    }

    public void setNombreColaborador(String nombreColaborador) {
        this.nombreColaborador = nombreColaborador;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}
