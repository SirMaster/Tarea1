package com.umg.tarea.clases;

/**
 * Created by Guicho on 5/07/2017.
 */
public class Actividad {
    private String nombreActividad;
    boolean actDentro;

    public Actividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public String getNombreActividad() {
        return nombreActividad;
    }

    public void setNombreActividad(String nombreActividad) {
        this.nombreActividad = nombreActividad;
    }

    public boolean isActDentro() {
        return actDentro;
    }

    public void setActDentro(boolean actDentro) {
        this.actDentro = actDentro;
    }
}
